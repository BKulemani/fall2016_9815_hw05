#include "FutureProductService.hpp"

// Constructor
// This will just create a map to hold the futures.
FutureProductService::FutureProductService()
{
	_futureMap = map<string, Future>();
}

// This will return a reference to a future requested by
// the product Id.
Future& FutureProductService::GetData(string productId)
{
	return _futureMap[productId];
}

// Adds a future to the alread created map.
void FutureProductService::Add(Future &Future_source)
{
	_futureMap.insert(pair<string, Future>(Future_source.GetProductId(), Future_source));
}

// Function to print the map
void FutureProductService::Print() const
{

	std::map<string, Future>::const_iterator it;
	int n = 1;
	for (it = _futureMap.begin(); it != _futureMap.end(); ++it)
	{
		if (n == 1) it->second.PrintHeader();
 		it->second.PrintData();
		n++;
	}
}
