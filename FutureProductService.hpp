// Bledar Kulemani
// This is a class derived from Service and it is a product services specifically 
// for futures. It allows users to create a container that holds the futures that
// will be used. This container is a map.

#ifndef FutureProductService_HPP
#define FutureProductService_HPP

#include "soa.hpp"
#include "Future.hpp"
#include <map>
#include <iterator>

class FutureProductService : public Service<string, Future>
{
public:
	// FutureProductServic constructor
	FutureProductService();

	// Return the future data for a particular future product identifier
	Future& GetData(string productId);

	// Add a future to the service
	void Add(Future &future_source);

	// Print a future
	virtual void Print() const;
private:
	map<string, Future> _futureMap; // cache of Future products
};
#endif // !FutureProductService_HPP
