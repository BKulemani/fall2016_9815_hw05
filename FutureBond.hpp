// Bledar Kulemani
// This is the interface of FutureBond class. 
// It is inhereted from Future class. In addition, it has another 
// data member and another method which are specific to the FutureBond
// class.

#ifndef FutureBond_HPP
#define FutureBond_HPP

#include "Future.hpp"
#include <iostream>

// The FutureBond class is inhereted from the Future Class. For simplicity, we will add
// one data member and one method to this class.

class FutureBond : public Future
{
protected:
	double _Price;		// The price of the future bond

public:
	FutureBond(string productId, string ticker, double Price, double InitialMargin, double MaintanceMargin, date ExpirationDate);
	double GetPrice() const;	// Returns the price of the future bond

	// Print function to print the future data and the future header
	virtual void PrintHeader() const override;

	virtual void PrintData() const override;
};

#endif // !FutureBond_HPP