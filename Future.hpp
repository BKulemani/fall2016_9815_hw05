// Bledar Kulemani
// The class Future is inhereted from the class Product.


#ifndef FUTURE_HPP
#define FUTURE_HPP

#include "products.hpp"
#include <iomanip>
#include "boost/date_time/gregorian/gregorian.hpp"

using namespace std;
using namespace boost::gregorian;


class Future : public Product
{
public:

	// Default constructor
	Future();

	// Constructor with parameters
	Future(string productId, string ticker, double InitialMargin, double MaintanceMargin, date ExpirationDate);

	// Destructor
	~Future();

	// Return the Id of the product
	string GetProductId() const;

	// Return the ticket of the future
	string GetTicker() const;

	// Return the InitialMargin of the future
	double GetInitialMargin() const;

	// Return the Maintenance Margin of the future
	double GetMaintenanceMargin() const;

	// Return the maturity date of the maturity
	date GetExpirationDate() const;

	// Print function to print the future data and the future header
	virtual void PrintHeader() const;

	virtual void PrintData() const;

	// Note:
	// These variables are declared as protected so that the classes which
	// will be inhereted from Future, will be able to use these variables.

protected:
	string _ProductId;			// variable that identifies the product
	string _Ticker;				// variable that identifies the issuer
	double _InitialMargin;		// variable that identifies the amount needed to start with
	double _MaintenanceMargin;	// variable that identifies the amount needed to start with
	date _ExpirationDate;		// variable that identifies the maturity date
};

#endif // !future_HPP
