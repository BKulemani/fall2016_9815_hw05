// Bledar Kulemani
// Implementation of the class EuroDollarFuture


#include "EuroDollarFuture.hpp"
//#include <string>
// The constructor; We are calling the future constructor, and also initializing the price.
EuroDollarFuture::EuroDollarFuture(string productId, string ticker, double TickSize, double InitialMargin, double MaintanceMargin, date ExpirationDate)
	: Future(productId, ticker, InitialMargin, MaintanceMargin, ExpirationDate), _TickSize(TickSize) {
	;
}

// The function that returns the tick size of the eurodollar future
double EuroDollarFuture::GetTickSize() const
{
	return _TickSize;
}

// Print funtions
void EuroDollarFuture::PrintHeader() const
{
	std::cout << right << setw(15) << "Product Id" << setw(15) << " Ticker" << setw(15) << "Tick Size" << setw(15) << " Init Margin" << setw(15) << " Maint Margin" << std::endl;
}
void EuroDollarFuture::PrintData() const
{
	std::cout << right;
	std::cout << setw(15) << _ProductId << setw(15) << _Ticker << setw(15) << _TickSize << setw(15) << _InitialMargin << setw(15) << _MaintenanceMargin << std::endl;
}
