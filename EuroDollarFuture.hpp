// Bledar Kulemani
// This is the interface of EuroDollar future class. 
// It is inhereted from Future class. In addition, it has another 
// data member and another method which are specific to the FutureBond
// class.

#ifndef EuroDollarFuture_HPP
#define EuroDollarFuture_HPP

#include "Future.hpp"
#include <iostream>
#include "boost/date_time/gregorian/gregorian.hpp"

using namespace std;
using namespace boost::gregorian;


// The FutureBond class is inhereted from the Future Class. For simplicity, we will add
// one data member and one method to this class.

class EuroDollarFuture : public Future
{
protected:
	double _TickSize;		// The price of the future bond

public:
	EuroDollarFuture(string productId, string ticker, double TickSize, double InitialMargin, double MaintanceMargin, date ExpirationDate);
	double GetTickSize() const;	// Returns the tick size of the euro dollar future, example 0.0025 a quarter of 1%

	// Print function to print the future data and the future header
	virtual void PrintHeader() const override;

	virtual void PrintData() const override;
};

#endif // !EuroDollarFuture_HPP
