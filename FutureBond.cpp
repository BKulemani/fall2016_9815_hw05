// Bledar Kulemani
// Implementation of FutureBond class

#include "FutureBond.hpp"

// The constructor; We are calling the future constructor, and also initializing the price.
FutureBond::FutureBond(string productId, string ticker, double Price, double InitialMargin, double MaintanceMargin, date ExpirationDate)
	: Future(productId, ticker, InitialMargin, MaintanceMargin, ExpirationDate), _Price(Price) {;}

// The function that returns the price of the future bond
double FutureBond::GetPrice() const
{
	return _Price;
}

// Print funtions
void FutureBond::PrintHeader() const 
{
	std::cout << right << setw(15) << "Product Id" << setw(15) << " Ticker" << setw(15) << "Price" << setw(15) << " Init Margin" << setw(15) << " Maint Margin" << std::endl;
}
void FutureBond::PrintData() const
{
	std::cout << right;
	std::cout << setw(15) << _ProductId << setw(15) << _Ticker << setw(15) << _Price << setw(15) << _InitialMargin << setw(15) << _MaintenanceMargin << std::endl;
}
