#ifndef productservice_HPP
#define productservice_HPP


/**
* productservice.hpp defines Bond and IRSwap ProductServices
*/

#include <iostream>
#include <map>
#include <vector>
#include "products.hpp"
#include "soa.hpp"

/**
* Bond Product Service to own reference data over a set of bond securities.
* Key is the productId string, value is a Bond.
*/
class BondProductService : public Service<string, Bond>
{

public:
	// BondProductService ctor
	BondProductService();

	// Return the bond data for a particular bond product identifier
	Bond& GetData(string productId);

	// Add a bond to the service (convenience method)
	void Add(Bond &bond);

	// Functions for the third problem
	// Function that returns a vector of bonds, when the ticker is given.
	std::vector<Bond> GetBonds(string& TickerSource);

private:
	map<string, Bond> bondMap; // cache of bond products

};

/**
* Interest Rate Swap Product Service to own reference data over a set of IR Swap products
* Key is the productId string, value is a IRSwap.
*/
class IRSwapProductService : public Service<string, IRSwap>
{
public:
	// IRSwapProductService ctor
	IRSwapProductService();

	// Return the IR Swap data for a particular bond product identifier
	IRSwap& GetData(string productId);

	// Add a bond to the service (convenience method)
	void Add(IRSwap &swap);

	// Functions for the third problem
	// Note that the parameters passed to these functions have different
	// types of parameters. Therefore, we can use overload these functions.
	// However, I will write down specific different names here so that it is
	// easy to note them.

	// A function to print all the swaps
	void Print();

	// Swaps with specified fixed leg count day convention
	std::vector<IRSwap> GetSwaps(DayCountConvention FixedDayCountConvention);

	// Swaps with specified fixed leg payment frequency
	std::vector<IRSwap> GetSwaps(PaymentFrequency FixedLegPaymentFrequency);

	// Swaps with specified floating index
	vector<IRSwap> GetSwaps(FloatingIndex FloatingIndex);

	// Swaps with a term in years greater than the specified value
	vector<IRSwap> GetSwapsGreaterThan(int TermYears);

	// Swaps with a term in years less than the specified value
	vector<IRSwap> GetSwapsLessThan(int TermYears);

	// Swaps with the specified swap type
	vector<IRSwap> GetSwaps(SwapType SwapType);

	// Swaps with the specified swap leg type
	vector<IRSwap> GetSwaps(SwapLegType SwapLegType);

private:
	map<string, IRSwap> swapMap; // cache of IR Swap products

};
#endif // !productservice_HPP
