

//#ifndef future_CPP
//#define future_CPP

#include "Future.hpp"


Future::Future() : Product(0, FUTURE) {}
Future::Future(string productId, string ticker, double InitialMargin, double MaintanceMargin, date ExpirationDate) : Product(productId, FUTURE)
{
	_ProductId = productId;
	_Ticker = ticker;
	_InitialMargin = InitialMargin;
	_MaintenanceMargin = MaintanceMargin;
	_ExpirationDate = ExpirationDate;
}

// Destructor
Future::~Future() { ; } // This is specified explicitely 

// Returns the product id
string Future::GetProductId() const
{
	return _ProductId;
}

// Returns the initial margin
double Future::GetInitialMargin() const
{
	return _InitialMargin;
}

// Returns the maintenance margin
double Future::GetMaintenanceMargin() const
{
	return _MaintenanceMargin;
}

// Returns the ticker of the future
string Future::GetTicker() const
{
	return _Ticker;
}

// Returns the experation date
date Future::GetExpirationDate() const
{
	return _ExpirationDate;
}

void Future::PrintHeader() const
{
	std::cout << right << setw(15)  << "Product Id" << setw(15) << " Ticker" << setw(15) << " Init Margin" << setw(15) << " Maint Margin" << std::endl;
}
void Future::PrintData() const
{
	std::cout << right;
	std::cout  << setw(15) << _ProductId << setw(15) << _Ticker << setw(15) << _InitialMargin << setw(15) << _MaintenanceMargin <<  std::endl;
}

//#endif // !future_CPP



