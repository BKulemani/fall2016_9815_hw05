Note: Problems 2 and 3 are tested on the same cpp file.
Also note that the professor's code is altered as follows:
- The interface is seperated from the implementation
- There are added functionality to the classes so that the testing is done more naturaly

Regarding problems 2 and 3, you will find 14 .hpp and .cpp files, correspoding to Product,
ProductService, Bonds, Interest Rate Swaps, Futures. The Bonds, Interest Rate Swaps and the 
Futures are derived from Products. Additionally, from Futures there are derived two other 
classes, FutureBonds and EuroDollar Future.  

How to run it?
	The testing file testing.cpp needs to be run. Note that the code uses boost library.
Therefore, it must be included if if it is run from the console.

What is being tested?
	In general terms we are creating financial products like Bonds, Interest Rate Swaps,
and Futures. Additionally, we are creating specific type of Futures, like EuroDollar and 
Bond Future. Then, we create an class which is in "service" of the financial products.
This product-service object adds, stores, and prints prints the financial products based
on the different parameters provided by the user. The testing file is supposed to test
at least all the new functionalities of the products and the product service. 



