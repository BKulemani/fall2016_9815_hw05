#include "products.hpp"

Product::Product(string _productId, ProductType _productType)
{
	productId = _productId;
	productType = _productType;
}

string Product::GetProductId() const
{
	return productId;
}

ProductType Product::GetProductType() const
{
	return productType;
}

Bond::Bond(string _productId, BondIdType _bondIdType, string _ticker, float _coupon, date _maturityDate) : Product(_productId, BOND)
{
	bondIdType = _bondIdType;
	ticker = _ticker;
	coupon = _coupon;
	maturityDate = _maturityDate;
}

Bond::Bond() : Product(0, BOND)
{
}

string Bond::GetTicker() const
{
	return ticker;
}

float Bond::GetCoupon() const
{
	return coupon;
}

date Bond::GetMaturityDate() const
{
	return maturityDate;
}

BondIdType Bond::GetBondIdType() const
{
	return bondIdType;
}

ostream& operator<<(ostream &output, const Bond &bond)
{
	output << bond.ticker << " " << bond.coupon << " " << bond.GetMaturityDate();
	return output;
}

// This is the implementation of the function that prints a bond
void Bond::Print() 
{
	std::cout << right << setw(15) << Product::GetProductId() << setw(10) << bondIdType
			<< setw(20) << ticker << setw(15) << coupon << setw(15) 
		<<  std::endl;
}


IRSwap::IRSwap(string _productId, DayCountConvention _fixedLegDayCountConvention, DayCountConvention _floatingLegDayCountConvention, PaymentFrequency _fixedLegPaymentFrequency, FloatingIndex _floatingIndex, FloatingIndexTenor _floatingIndexTenor, date _effectiveDate, date _terminationDate, Currency _currency, int _termYears, SwapType _swapType, SwapLegType _swapLegType) : Product(_productId, IRSWAP)
{
	fixedLegDayCountConvention = _fixedLegDayCountConvention;
	floatingLegDayCountConvention = _floatingLegDayCountConvention;
	fixedLegPaymentFrequency = _fixedLegPaymentFrequency;
	floatingIndex = _floatingIndex;
	floatingIndexTenor = _floatingIndexTenor;
	effectiveDate = _effectiveDate;
	terminationDate = _terminationDate;
	currency = _currency;
	termYears = _termYears;
	swapType = _swapType;
	swapLegType = _swapLegType;
}

IRSwap::IRSwap() : Product(0, IRSWAP)
{
}

DayCountConvention IRSwap::GetFixedLegDayCountConvention() const
{
	return fixedLegDayCountConvention;
}

DayCountConvention IRSwap::GetFloatingLegDayCountConvention() const
{
	return floatingLegDayCountConvention;
}

PaymentFrequency IRSwap::GetFixedLegPaymentFrequency() const
{
	return fixedLegPaymentFrequency;
}

FloatingIndex IRSwap::GetFloatingIndex() const
{
	return floatingIndex;
}

FloatingIndexTenor IRSwap::GetFloatingIndexTenor() const
{
	return floatingIndexTenor;
}

date IRSwap::GetEffectiveDate() const
{
	return effectiveDate;
}

date IRSwap::GetTerminationDate() const
{
	return terminationDate;
}

Currency IRSwap::GetCurrency() const
{
	return currency;
}

int IRSwap::GetTermYears() const
{
	return termYears;
}

SwapType IRSwap::GetSwapType() const
{
	return swapType;
}

SwapLegType IRSwap::GetSwapLegType() const
{
	return swapLegType;
}


ostream& operator<<(ostream &output, const IRSwap &swap)
{
	output << "fixedDayCount:" << swap.ToString(swap.GetFixedLegDayCountConvention()) << " floatingDayCount:" << swap.ToString(swap.GetFloatingLegDayCountConvention()) << " paymentFreq:" << swap.ToString(swap.GetFixedLegPaymentFrequency()) << " " << swap.ToString(swap.GetFloatingIndexTenor()) << swap.ToString(swap.GetFloatingIndex()) << " effective:" << swap.GetEffectiveDate() << " termination:" << swap.GetTerminationDate() << " " << swap.ToString(swap.GetCurrency()) << " " << swap.GetTermYears() << "yrs " << swap.ToString(swap.GetSwapType()) << " " << swap.ToString(swap.GetSwapLegType());
	return output;
}

string IRSwap::ToString(DayCountConvention dayCountConvention) const
{
	switch (dayCountConvention) {
	case THIRTY_THREE_SIXTY: return "30/360";
	case ACT_THREE_SIXTY: return "Act/360";
	default: return "";
	}
}

string IRSwap::ToString(PaymentFrequency paymentFrequency) const
{
	switch (paymentFrequency) {
	case QUARTERLY: return "Quarterly";
	case SEMI_ANNUAL: return "Semi-Annual";
	case ANNUAL: return "Annual";
	default: return "";
	}
}

string IRSwap::ToString(FloatingIndex floatingIndex) const
{
	switch (floatingIndex) {
	case LIBOR: return "LIBOR";
	case EURIBOR: return "EURIBOR";
	default: return "";
	}
}

string IRSwap::ToString(FloatingIndexTenor floatingIndexTenor) const
{
	switch (floatingIndexTenor) {
	case TENOR_1M: return "1m";
	case TENOR_3M: return "3m";
	case TENOR_6M: return "6m";
	case TENOR_12M: return "12m";
	default: return "";
	}
}

string IRSwap::ToString(Currency currency) const
{
	switch (currency) {
	case USD: return "USD";
	case EUR: return "EUR";
	case GBP: return "GBP";
	default: return "";
	}
}

string IRSwap::ToString(SwapType swapType) const
{
	switch (swapType) {
	case SPOT: return "Standard";
	case FORWARD: return "Forward";
	case IMM: return "IMM";
	case MAC: return "MAC";
	case BASIS: return "Basis";
	default: return "";
	}
}

string IRSwap::ToString(SwapLegType swapLegType) const
{
	switch (swapLegType) {
	case OUTRIGHT: return "Outright";
	case CURVE: return "Curve";
	case FLY: return "Fly";
	default: return "";
	}
}

// This is the implementation of the function that prints a Interest Rate Swap
void IRSwap::Print()
{
	// Making enum print out text
	string fixed_day_count_convention;
	if (fixedLegDayCountConvention == 0)
		fixed_day_count_convention = "THIRTY_THREE_SIXTY";
	else if (fixedLegDayCountConvention == 1)
		fixed_day_count_convention = "ACT_THREE_SIXTY";
	else
		fixed_day_count_convention = "ACT_THREE_SIXTY_FIVE";

	string fixed_payment_frequency;
	if (fixedLegPaymentFrequency == 0)
		fixed_payment_frequency = "QUARTERLY";
	else if (fixedLegPaymentFrequency == 1)
		fixed_payment_frequency = "SEMI_ANNUAL";
	else fixed_payment_frequency = "ANNUAL";

	string floating_index;
	if (floatingIndex == 0)
		floating_index = "LIBOR";
	else
		floating_index = "EURIBOR";

	string swap_type;
	if (swapType == 0)
		swap_type = "SPOT";
	else if (swapType == 1)
		swap_type = "FORWARD";
	else if (swapType == 2)
		swap_type = "IMM";
	else if (swapType == 3)
		swap_type = "MAC";
	else swap_type = "BASIC";

	string swap_leg_type;
	if (swapLegType == 0)
		swap_leg_type = "OUTRIGHT";
	else if (swapLegType == 1)
		swap_leg_type = "CURVE";
	else
		swap_leg_type = "FLY";

	std::cout << right << setw(20) << Product::GetProductId() << setw(25) << fixed_day_count_convention
		<< setw(15) << fixed_payment_frequency <<setw(10) 
		<< setw(15) << floating_index << setw(10) << termYears 
		<< setw(10) << swap_type << setw(15) << swap_leg_type << std::endl;
}
