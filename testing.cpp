#include <iostream>
#include "products.hpp"
#include "products.cpp"
#include "productservice.hpp"
#include "productservice.cpp"
#include "Future.hpp"
#include "Future.cpp"
#include "EuroDollarFuture.hpp"
#include "EuroDollarFuture.cpp"
#include "FutureBond.hpp"
#include "FutureBond.cpp"
#include "FutureProductService.hpp"
#include "FutureProductService.cpp"
#include "boost/date_time/gregorian/gregorian.hpp"

//using namespace std;
//using namespace boost::gregorian;


// A template function to print a vector.
template <typename T>
void PrintVector(T vector_source){
	int size = vector_source.size();
	for (int i = 0; i < size; ++i)
		vector_source[i].Print();
};


int main()
{
	std::cout << "----------------------------------------------------------------------------------------------------------------\n";
	std::cout << "----------------------------------------------     Problem 2      ----------------------------------------------\n";
	std::cout << "----------------------------------------------------------------------------------------------------------------\n\n\n";

	std::cout << "***********   Regular Future Contracts   **************\n\n";
	// Problem 2 
	// We first add two futures. Then, we print it.
	// Future 1
	date date1(2026, Nov, 24);
	Future Future1("ID 1", "USA", 600, 301, date1);
	
	// Future 2
	date date2(2026, Nov, 25);
	Future Future2("ID 2", "Japan", 500, 299, date2);

	// Creating a FutureProductService class, more specifically, a pointer to a class
	FutureProductService * my_futureProductService = new FutureProductService();
	
	// Adding the futures to our service container
	my_futureProductService->Add(Future1);
	my_futureProductService->Add(Future2);

	// Printing the futures.
	std::cout << "After adding 2 futures, the future service contains:\n\n";
	my_futureProductService->Print();
	std::cout << std::endl << std::endl;

	// Adding a third future contract
	// Future 3
	date date3(2026, Nov, 26);
	Future Future3("ID 4", "Germany", 450, 251, date3);
	my_futureProductService->Add(Future3);

	// Printing the futures.
	std::cout << "After adding another future, the future service contains:\n\n";
	my_futureProductService->Print();
	std::cout << std::endl << std::endl;


	std::cout << "***********   EuroDollar Future Contracts   **************\n\n";
	// EuroDollar Future 1
	date date4(2026, Nov, 27);
	EuroDollarFuture EuroDollarFuture1("ID 10", "USA", 0.04, 600, 301, date4);
	
	// Now, we test the polymorphic behavior of the classes
	EuroDollarFuture1.PrintHeader();
	EuroDollarFuture1.PrintData();
	std::cout << std::endl << std::endl;


	std::cout << "***********   Bond Future Contracts   **************\n\n";
	// Bond Future 1
	date date6(2026, Nov, 28);

	FutureBond BondFuture1("ID 10", "USA", 200, 600, 301, date6);

	// Now, we test the polymorphic behavior of the classes
	BondFuture1.PrintHeader();
	BondFuture1.PrintData();
	std::cout << std::endl << std::endl;


	std::cout << "----------------------------------------------------------------------------------------------------------------\n";
	std::cout << "----------------------------------------------     Problem 3      ----------------------------------------------\n";
	std::cout << "----------------------------------------------------------------------------------------------------------------\n\n\n";

	// For the bond testing, we will add 4 bonds where 3 are T notes and
	// 1 is T- bill. Then, we will print out the bonds based on their ticker
	// and see what the results are. We should expect 3 T-Notes together 
	// and 1 T-Bill on its own.

	// Create the 10Y treasury note
	date maturityDate(2026, Nov, 26);
	string cusip = "912828M56";
	Bond treasuryBond(cusip, CUSIP, "Treasury Note", 2.25, maturityDate);

	// Create the 5Y treasury note
	date maturityDate2(2018, Nov, 21);
	string cusip2 = "912828TW0";
	Bond treasuryBond2(cusip2, CUSIP, "Treasury Note", 2.75, maturityDate2);

	// Create the 10Y treasury bills
	date maturityDate3(2018, Nov, 27);
	string cusip3 = "912828TW1";
	Bond treasuryBond3(cusip3, CUSIP, "Treasury Bill", 0.75, maturityDate3);

	// Create the 7Y treasury note
	date maturityDate4(2018, Nov, 21);
	string cusip4 = "912828TW2";
	Bond treasuryBond4(cusip4, ISIN, "Treasury Note", 1.85, maturityDate4);
	// Create a BondProductService
	BondProductService *bondProductService = new BondProductService();

	// Add the Bonds to the BondProductService and retrieve it from the service
	bondProductService->Add(treasuryBond);
	bondProductService->Add(treasuryBond2);
	bondProductService->Add(treasuryBond3);
	bondProductService->Add(treasuryBond4);

	std::cout <<"\t\t\t" << "******* Bonds ********\n\n";
	string Note = "Treasury Note";
	string Bill = "Treasury Bill";
	std::cout << "The bonds with a ticker of '" << Note << "' are:\n";
	PrintVector(bondProductService->GetBonds(Note));	// Getting the Bonds are that Treasury Notes
	std::cout << std::endl << std::endl;

	std::cout << "The bonds with a ticker of '" << Bill << "' are:\n";
	PrintVector(bondProductService->GetBonds(Bill));	// Getting the Bonds are that Treasury Bills
	std::cout << std::endl << std::endl;


	 // Now we test the new funtionality of the function of IR swaps
	 // We first create some Interest Rate Swap contracts. 
	 // Then, we add that to the product service and finally, we print the results.

	std::cout << "\t\t" << "******* Interest Rate Swaps ********\n\n";

	// Creation of the IR Swaps
	// Create the Spot 10Y Outright Swap
	date effectiveDate(2016, Nov, 16);
	date terminationDate(2026, Nov, 16);
	string outright10Y = "Spot-Outright-10Y";
	IRSwap outright10YSwap(outright10Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate, terminationDate, USD, 10, SPOT, OUTRIGHT);

	// Create the IMM 2Y Outright Swap
	date effectiveDate2(2016, Dec, 20);
	date terminationDate2(2018, Dec, 20);
	string imm2Y = "IMM-Outright-2Y";
	IRSwap imm2YSwap(imm2Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate2, terminationDate2, USD, 2, IMM, OUTRIGHT);

	// Create the IMM 2Y CURVE Swap
	date effectiveDate3(2016, Dec, 26);
	date terminationDate3(2018, Dec, 25);
	string imm2Y2 = "IMM-Outright-2Y2";
	IRSwap imm2YSwap2(imm2Y2, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate3, terminationDate3, USD, 2, IMM, CURVE);

	// Create the IMM 5Y OUTRIGHT Swap
	date effectiveDate4(2016, Dec, 27);
	date terminationDate4(2018, Dec, 27);
	string imm5Y = "IMM-CURVE-5Y";
	IRSwap imm5YSwap(imm5Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate4, terminationDate4, USD, 5, IMM, OUTRIGHT);
	
	// Create the Spot 5Y Outright Swap
	date effectiveDate5(2017, Nov, 16);
	date terminationDate5(2022, Nov, 16);
	string outright5Y = "Spot-Outright-5Y";
	IRSwap outright5YSwap(outright5Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate5, terminationDate5, USD, 5, SPOT, OUTRIGHT);

	// Create the Spot 20Y Outright Swap
	date effectiveDate6(2017, Nov, 16);
	date terminationDate6(2037, Nov, 16);
	string outright20Y = "Spot-Outright-20Y";
	IRSwap outright20YSwap(outright20Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, QUARTERLY, LIBOR, TENOR_3M, effectiveDate6, terminationDate6, USD, 20, SPOT, OUTRIGHT);

	// Create the Spot 25Y Outright Swap
	date effectiveDate7(2017, Nov, 16);
	date terminationDate7(2042, Nov, 16);
	string outright25Y = "Spot-Outright-25Y";
	IRSwap outright25YSwap(outright25Y, ACT_THREE_SIXTY, THIRTY_THREE_SIXTY, ANNUAL, EURIBOR, TENOR_3M, effectiveDate7, terminationDate7, USD, 5, SPOT, OUTRIGHT);	// Notice the EURIBOR

	// Create a IRSwapProductService
	IRSwapProductService *swapProductService = new IRSwapProductService();

	// Adding the IR Swaps to the product service
	swapProductService->Add(outright10YSwap);
	swapProductService->Add(imm2YSwap);
	swapProductService->Add(imm2YSwap2);
	swapProductService->Add(imm5YSwap);
	swapProductService->Add(outright20YSwap);
	swapProductService->Add(outright25YSwap);

	// We first print out all IR Swaps:
	std::cout << "The IR swap service contains:\n\n";
	swapProductService->Print();
	std::cout << std::endl << std::endl;

	// We now print the IR swaps that have "ACT_THREE_SIXTY" day-count-convention
	//ACT_THREE_SIXTY counting days
	std::cout << "The IR swap with 'ACT_THREE_SIXTY' day count convention for fixed leg are:\n\n";
	PrintVector(swapProductService->GetSwaps(ACT_THREE_SIXTY));		// Only 1 expected
	std::cout << std::endl << std::endl;

	//THIRTY_THREE_SIXTY counting days
	std::cout << "The IR swap with 'THIRTY_THREE_SIXTY' day count convention for fixed leg are:\n\n";
	PrintVector(swapProductService->GetSwaps(THIRTY_THREE_SIXTY));	// The other 5 expected
	std::cout << std::endl << std::endl;

	// Fixed leg payment frequency- ANNUAL
	std::cout << "The IR swap with fixed leg payment frequency of 'ANNUALLY' are:\n\n";
	PrintVector(swapProductService->GetSwaps(ANNUAL));	// Only 1 expected
	std::cout << std::endl << std::endl;

	// Fixed leg payment frequency - Semi-Annual
	std::cout << "The IR swap with fixed leg payment frequency of 'SEMI_ANNUAL' are:\n\n";
	PrintVector(swapProductService->GetSwaps(SEMI_ANNUAL)); // 4 expected
	std::cout << std::endl << std::endl;

	// Fixed leg payment frequency - Quartely
	std::cout << "The IR swap with fixed leg payment frequency of 'QUARTERLY' are:\n\n";
	PrintVector(swapProductService->GetSwaps(QUARTERLY));	// Only 1 expected
	std::cout << std::endl << std::endl;

	// Swaps with a term greater than 7
	std::cout << "The IR swap with a term greater than 7 are:\n\n";
	PrintVector(swapProductService->GetSwapsGreaterThan(7));	// Only 2 expected
	std::cout << std::endl << std::endl;

	// Swaps with a term less than 7
	std::cout << "The IR swap with a term less than 7 are:\n\n";
	PrintVector(swapProductService->GetSwapsLessThan(7));	// The other 4 expected
	std::cout << std::endl << std::endl;

	// Swaps of a type SPOT
	std::cout << "The IR swap of the type SPOT are:\n\n";
	PrintVector(swapProductService->GetSwaps(SPOT));	// 3 expected
	std::cout << std::endl << std::endl;

	// Swaps of a type IMM
	std::cout << "The IR swap of the type IMM are:\n\n";
	PrintVector(swapProductService->GetSwaps(IMM));	// 3 expected
	std::cout << std::endl << std::endl;

	// Swaps with lef of type OUTRIGHT
	std::cout << "The IR swap with leg of the type OUTRIGHT are:\n\n";
	PrintVector(swapProductService->GetSwaps(OUTRIGHT));	// 5 expected
	std::cout << std::endl << std::endl;

	// Swaps with lef of type CURVE
	std::cout << "The IR swap with leg of the type CURVE are:\n\n";
	PrintVector(swapProductService->GetSwaps(CURVE));	// 1 expected
	std::cout << std::endl << std::endl;

	return 0;
}
