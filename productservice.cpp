#include "productservice.hpp"

BondProductService::BondProductService()
{
	bondMap = map<string, Bond>();
}

Bond& BondProductService::GetData(string productId)
{
	return bondMap[productId];
}

void BondProductService::Add(Bond &bond)
{
	bondMap.insert(pair<string, Bond>(bond.GetProductId(), bond));
}

IRSwapProductService::IRSwapProductService()
{
	swapMap = map<string, IRSwap>();
}

IRSwap& IRSwapProductService::GetData(string productId)
{
	return swapMap[productId];
}

void IRSwapProductService::Add(IRSwap &swap)
{
	swapMap.insert(pair<string, IRSwap>(swap.GetProductId(), swap));
}

// Functions related to Problem 3
// For the bond
std::vector<Bond> BondProductService::GetBonds(string& TickerSource) 
{
	// Create an empty vector
	std::vector<Bond> temp_vector;

	// We now iterate through the map and collect those records that contain
	// the ticker provided.
	std::map<string, Bond>::const_iterator it;
	for (it = bondMap.cbegin(); it != bondMap.cend(); ++it)
	{
		if (it->second.GetTicker() == TickerSource)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// For the Interest Rate Swaps

// Swaps with specified fixed leg count day convention
std::vector<IRSwap> IRSwapProductService::GetSwaps(DayCountConvention FixedDayCountConvention)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetFixedLegDayCountConvention() == FixedDayCountConvention)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// Swaps with specified fixed leg payment frequency
std::vector<IRSwap> IRSwapProductService::GetSwaps(PaymentFrequency FixedLegPaymentFrequency)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetFixedLegPaymentFrequency() == FixedLegPaymentFrequency)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// Swaps with specified floating index
std::vector<IRSwap> IRSwapProductService::GetSwaps(FloatingIndex FloatingIndex)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetFloatingIndex() == FloatingIndex)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// Swaps with a term in years greater than the specified value
std::vector<IRSwap> IRSwapProductService::GetSwapsGreaterThan(int TermYears)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetTermYears() > TermYears)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// Swaps with a term in years smaller than the specified value
std::vector<IRSwap> IRSwapProductService::GetSwapsLessThan(int TermYears)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetTermYears() < TermYears)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// Swaps with the specified swap type
std::vector<IRSwap> IRSwapProductService::GetSwaps(SwapType SwapType)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetSwapType()  == SwapType)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

// Swaps with the specified swap leg type
std::vector<IRSwap> IRSwapProductService::GetSwaps(SwapLegType SwapLegType)
{
	// Create an empty vector
	std::vector<IRSwap> temp_vector;

	// We now iterate through the map and collect those IR swaps that 
	// have a fixed leg day count convention requested.

	std::map<string, IRSwap>::const_iterator it;
	for (it = swapMap.cbegin(); it != swapMap.cend(); ++it)
	{
		if (it->second.GetSwapLegType() == SwapLegType)
			temp_vector.push_back(it->second);	// Add to the vector
	}
	return temp_vector;
}

void IRSwapProductService::Print()
{
	std::map<string, IRSwap>::iterator it;
	for (it = swapMap.begin(); it != swapMap.end(); ++it)
	{
		it->second.Print();	
	}
}
