//Kristin Zhang
//#include "stdafx.h"
#include "boost/interprocess/managed_shared_memory.hpp"
#include <iostream>

using namespace std;
using namespace boost::interprocess;

int main()
{
	shared_memory_object::remove("Boost1");
	shared_memory_object::remove("Boost2");


	//create a managed_shared_memory with size 1024,named Boost;
	managed_shared_memory managed_shm1{ open_or_create, "Boost1", 1024 };
	managed_shared_memory managed_shm2{ open_or_create, "Boost2", 1024 };

	//assign the value of the memory with an integer 4
	int *i1 = managed_shm1.construct<int>("Integer")(4);
	int *i2 = managed_shm2.construct<int>("Integer")(5);
}
