#Kristin Zhang
#include "stdafx.h"
#include "boost/interprocess/managed_shared_memory.hpp"
#include <iostream>

using namespace std;
using namespace boost::interprocess;

int main()
{
	//open the managed memory
	managed_shared_memory managed_shm1{open_or_create, "Boost1", 1024 };
	managed_shared_memory managed_shm2{ open_or_create, "Boost2", 1024 };

	//find the integer
	std::pair<int*, std::size_t> p1 = managed_shm1.find<int>("Integer");
	std::pair<int*, std::size_t> p2 = managed_shm2.find<int>("Integer");


	//print the value of the integers and their multiplication
	cout << *p1.first <<endl;
	cout << *p2.first <<endl;
	cout << (*p1.first)*(*p2.first) << endl;

	return 0;
}